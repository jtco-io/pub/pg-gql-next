import "../styles/globals.css";
import type { AppProps } from "next/app";
import SiteLayout from "@components/SiteLayout";
import { ApolloProvider, useApollo } from "@utils/apollo";
import { useViewerObservable } from "@hooks/useViewer";
import { useRouter } from "next/router";

function App({
  Component,
  pageProps: { initialApolloState, ...pageProps },
}: AppProps) {
  const router = useRouter();
  const apolloClient = useApollo(initialApolloState);
  const [viewer, ViewerContext] = useViewerObservable(apolloClient);
  return (
    <ApolloProvider client={apolloClient}>
      <ViewerContext.Provider value={viewer}>
        <SiteLayout>
          {router.isReady && <Component {...pageProps} />}
        </SiteLayout>
      </ViewerContext.Provider>
    </ApolloProvider>
  );
}
export default App;
