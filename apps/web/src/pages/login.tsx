import React from "react";
import { NextPage } from "next";
import { useRouter } from "next/router";
import PageLayout from "@components/PageLayout";
import ViewerLogin from "@routes/ViewerLogin";
import useViewer from "../hooks/useViewer";
import routeMap from "@routeMap";

const LoginPage: NextPage = () => {
  const router = useRouter();
  const viewer = useViewer();
  if (viewer) {
    router.push(routeMap.account.profile.href);
  }
  return (
    <PageLayout title="Login">
      <ViewerLogin />
    </PageLayout>
  );
};

export default LoginPage;
