import React from "react";
import { NextPage } from "next";
import { useRouter } from "next/router";
import PageLayout from "@components/PageLayout";
import ViewerRegistration from "@routes/ViewerRegistration";
import useViewer from "../hooks/useViewer";
import routeMap from "@routeMap";

const RegisterPage: NextPage = () => {
  const router = useRouter();
  const viewer = useViewer();
  if (viewer) {
    router.push(routeMap.account.profile.href);
  }
  return (
    <PageLayout title="Register">
      <ViewerRegistration />
    </PageLayout>
  );
};

export default RegisterPage;
