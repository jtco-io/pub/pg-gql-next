import { NextPage } from "next";
import PageLayout from "../../components/PageLayout";
import React from "react";
import useAdmin from "../../hooks/useAdmin";
import AdminIndex from "../../routes/AdminIndex/AdminIndex";

const AdminPage: NextPage = () => {
  useAdmin();
  return (
    <PageLayout title="Admin">
      <AdminIndex />
    </PageLayout>
  );
};

export default AdminPage;
