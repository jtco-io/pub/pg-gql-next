import { NextPage } from "next";
import PageLayout from "../../../components/PageLayout";
import React from "react";
import useAdmin from "../../../hooks/useAdmin";
import AdminOrganization from "../../../routes/AdminOrganization/AdminOrganization";

const AdminOrganizationPage: NextPage = () => {
  useAdmin();
  return (
    <PageLayout title="Admin: Organization">
      <AdminOrganization />
    </PageLayout>
  );
};

export default AdminOrganizationPage;
