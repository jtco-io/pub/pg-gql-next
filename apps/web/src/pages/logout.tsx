import React from "react";
import { NextPage } from "next";
import PageLayout from "../components/PageLayout";
import ViewerLogout from "../routes/ViewerLogout";

const Page: NextPage = () => {
  return (
    <PageLayout title="Logout">
      <h1>Logout</h1>
      <ViewerLogout />
    </PageLayout>
  );
};

export default Page;
