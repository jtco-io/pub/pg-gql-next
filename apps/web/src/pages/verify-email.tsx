import React from "react";
import { NextPage } from "next";
import PageLayout from "../components/PageLayout";
import ViewerVerifyEmail from "../routes/ViewerVerifyEmail";

const Page: NextPage = () => {
  return (
    <PageLayout title="Verify Email">
      <h1>Verify Email</h1>
      <ViewerVerifyEmail />
    </PageLayout>
  );
};

export default Page;
