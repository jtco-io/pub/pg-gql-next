import type { NextPage } from "next";
import PageLayout from "@components/PageLayout";

const Home: NextPage = () => {
  return <PageLayout title="Home">Home!</PageLayout>;
};

export default Home;
