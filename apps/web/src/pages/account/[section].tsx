import { NextPage } from "next";
import { ViewerProfile, ViewerEmails } from "@routes/ViewerAccount";
import React from "react";
import PageLayout from "@components/PageLayout";
import SidebarLayout from "@components/SidebarLayout";
import Link from "@components/Link";
import routeMap from "@routeMap";
import { useRouter } from "next/router";
import { useViewerRequired } from "@hooks/useViewerRequried";

const SidebarLinks = () => (
  <ul>
    <li>
      <Link href={routeMap.account.profile.href}>Profile</Link>
    </li>
    <li>
      <Link href={routeMap.account.emails.href}>Emails</Link>
    </li>
    <li>
      <Link href={routeMap.account.security.href}>Security</Link>
    </li>
  </ul>
);

function getSection(section: string): [React.FC, string] {
  switch (section) {
    case "profile":
      return [ViewerProfile, "Edit Profile"];
    case "emails":
      return [ViewerEmails, "Emails"];
  }
  return [() => <div>Page not Found</div>, "Page not found"];
}

const Page: NextPage = () => {
  useViewerRequired();
  const router = useRouter();
  const section = router.query.section as string;
  const [Component, title] = getSection(section);
  return (
    <SidebarLayout navChildren={<SidebarLinks />}>
      <PageLayout title={title as string}>
        <Component />
      </PageLayout>
    </SidebarLayout>
  );
};

export default Page;
