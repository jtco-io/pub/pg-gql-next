const routeMap = {
  landing: {
    href: "/",
  },
  account: {
    profile: { href: "/account/profile" },
    emails: { href: "/account/emails" },
    security: { href: "/account/security" },
  },
  admin: {
    index: {
      href: "/admin",
    },
    organization: (slug: string) => ({
      pathname: "/admin/organization/[slug]",
      query: { slug },
    }),
    organizations: { href: "/admin/organizations" },
  },
  auth: {
    registration: { href: "/register" },
    login: { href: "/login" },
    logout: { href: "/logout" },
  },
};

export default routeMap;
