import React from "react";

export const FormError: React.FC = (props) => <span>{props.children}</span>;

export const FormErrors: React.FC = (props) => <div>{props.children}</div>;
