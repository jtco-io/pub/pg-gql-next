import React from "react";

export interface FormProps {
  onSubmit?: () => void;
}

const Form: React.FC<FormProps> = (props) => (
  <form
    onSubmit={(e) => {
      e.preventDefault();
      props.onSubmit && props.onSubmit();
    }}
  >
    {props.children}
  </form>
);

export default Form;
