import React from "react";
import { default as NextLink, LinkProps as NextLinkProps } from "next/link";
import styles from "./Link.module.css";

type AnchorProps = React.DetailedHTMLProps<
  React.AnchorHTMLAttributes<HTMLAnchorElement>,
  HTMLAnchorElement
>;

export type LinkProps = NextLinkProps;

const Link: React.FC<LinkProps> = ({ children, href, ...props }) => (
  <NextLink href={href}>
    <a {...props} className={styles.root}>
      {children}
    </a>
  </NextLink>
);

export default Link;
