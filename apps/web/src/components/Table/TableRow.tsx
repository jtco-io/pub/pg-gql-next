import React, { ReactNode } from "react";
import styles from "./Table.module.css";

export interface TableRowProps {
  values: Record<string, string | ReactNode>;
  columnKeys: string[];
  keyColumn: string;
}

export const TableRow: React.FC<TableRowProps> = (props) => (
  <tr className={styles.tableRow}>
    {props.columnKeys.map((columnKey) => (
      <td key={`${props.values[props.keyColumn]}-${columnKey}`}>
        {props.values[columnKey]}
      </td>
    ))}
  </tr>
);

export default TableRow;
