import React from "react";
import { TableRow, TableRowProps } from "./TableRow";
import styles from "./Table.module.css";

export interface TableProps {
  columnKeys: string[];
  rows?: Array<TableRowProps["values"]>;
  keyColumn: string;
}

const Table: React.FC<TableProps> = ({ columnKeys, rows = [], keyColumn }) => (
  <table className={styles.table}>
    <thead className={styles.tableHead}>
      <tr className={styles.tableRow}>
        {columnKeys.map((column) => (
          <th key={column} scope="col" className="">
            {column}
          </th>
        ))}
      </tr>
    </thead>
    <tbody>
      {rows?.map((row, idx) => (
        <TableRow
          key={
            typeof row[keyColumn] === "string"
              ? (row[keyColumn] as string)
              : idx
          }
          columnKeys={columnKeys}
          values={row}
          keyColumn={keyColumn}
        />
      ))}
    </tbody>
  </table>
);

export default Table;
