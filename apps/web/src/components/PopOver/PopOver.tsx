import React, { useRef } from "react";
import styles from "./Popover.module.css";

const Popover: React.FC<{ id: string; text: string; className?: string }> = (
  props
) => {
  const ref = useRef<HTMLInputElement>(null);

  return (
    <div className={`${styles.root} ${props.className || ""}`}>
      <input
        id={props.id}
        className={styles.checkbox}
        type="checkbox"
        ref={ref}
      />
      <label htmlFor={props.id} className={styles.label}>
        {props.text}
      </label>
      <div className={styles.menu}>{props.children}</div>
      <div
        className={styles.interceptor}
        onClick={() => {
          if (ref.current) {
            ref.current.checked = !ref.current.checked;
          }
        }}
      />
    </div>
  );
};

export default Popover;
