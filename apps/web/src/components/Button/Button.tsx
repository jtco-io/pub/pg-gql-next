import React from "react";
import styles from "./Button.module.css";

type Variant = "text" | "contained" | "outlined";
type Color = "primary" | "secondary" | "success" | "error";

interface ButtonProps
  extends React.DetailedHTMLProps<
    React.ButtonHTMLAttributes<HTMLButtonElement>,
    HTMLButtonElement
  > {
  variant: Variant;
  color?: Color;
}

function toTitleCase(str: string) {
  return str.charAt(0).toUpperCase() + str.slice(1);
}

function getColorVariant(variant: Variant, color?: Color) {
  if (!color || color === "primary") {
    return `${styles.root} ${styles[variant]}`;
  }
  const colorClass = color.charAt(0).toUpperCase() + color.slice(1);
  return `${styles.root} ${styles[variant]} ${
    styles[`${variant}${colorClass}`]
  }`;
}

const Button: React.FC<ButtonProps> = ({ variant, color, ...props }) => (
  <button
    {...props}
    className={`${styles.root} ${styles[variant]} ${getColorVariant(
      variant,
      color
    )}`}
  >
    {props.children}
  </button>
);

export default Button;
