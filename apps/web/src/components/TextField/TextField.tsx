import React from "react";
import styles from "./TextField.module.css";

interface Props
  extends Omit<
    React.DetailedHTMLProps<
      React.InputHTMLAttributes<HTMLInputElement>,
      HTMLInputElement
    >,
    "onChange"
  > {
  label: string;
  value: string;
  onChange: (value: string) => void;
}

const TextField: React.FC<Props> = ({ label, onChange, ...inputProps }) => (
  <label className={styles.pureMaterialTextfieldFilled}>
    <input
      {...inputProps}
      onChange={({ target: { value } }) => onChange(value)}
      placeholder=" "
    />
    <span>{label}</span>
  </label>
);

export default TextField;
