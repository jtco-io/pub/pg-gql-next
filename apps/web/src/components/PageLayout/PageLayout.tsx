import React from "react";
import Head from "next/head";
import styles from "./PageLayout.module.css";

interface Props {
  title: string;
}

const PageLayout: React.FC<Props> = (props) => {
  return (
    <div className={styles.root}>
      <Head>
        <title>{props.title}</title>
      </Head>
      {props.children}
    </div>
  );
};

export default PageLayout;
