import React, { ReactNode } from "react";
import styles from "./SidebarLayout.module.css";

const SidebarLayout: React.FC<{ navChildren: ReactNode }> = (props) => (
  <div className={styles.root}>
    <nav className={styles.sideNav}>{props.navChildren}</nav>
    <div className={styles.body}>{props.children}</div>
  </div>
);

export default SidebarLayout;
