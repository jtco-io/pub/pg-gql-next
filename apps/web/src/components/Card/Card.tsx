import React from "react";
import styles from "./Card.module.css";

const Card: React.FC = (props) => (
  <div className={styles.root}>{props.children}</div>
);

export default Card;
