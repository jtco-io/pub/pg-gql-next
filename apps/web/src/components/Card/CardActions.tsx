import React, { CSSProperties } from "react";
import styles from "./Card.module.css";

const CardActions: React.FC<{ style?: CSSProperties }> = (props) => (
  <div className={styles.actions} style={props.style}>
    {props.children}
  </div>
);

export default CardActions;
