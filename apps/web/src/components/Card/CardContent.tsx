import React, { CSSProperties } from "react";
import styles from "./Card.module.css";

const CardContent: React.FC<{ style?: CSSProperties }> = (props) => (
  <div className={styles.content} style={props.style}>
    {props.children}
  </div>
);

export default CardContent;
