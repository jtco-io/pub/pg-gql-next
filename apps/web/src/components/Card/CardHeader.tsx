import React from "react";
import styles from "./Card.module.css";

const CardHeader: React.FC<{ title: string; subtitle?: string }> = (props) => (
  <div className={styles.header}>
    <div className={styles.headerContent}>
      <span>{props.title}</span>
      {props.subtitle && <span>{props.subtitle}</span>}
    </div>
  </div>
);

export default CardHeader;
