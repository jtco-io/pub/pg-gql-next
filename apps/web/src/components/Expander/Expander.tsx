import React from "react";
import styles from "./Expander.module.css";
import Chevron from "@components/Icons/Chevron";

const Expander: React.FC<{ id?: string; title: string }> = (props) => {
  const id = props.id ?? props.title.toLowerCase().replace(" ", "-");

  return (
    <div className={styles.root}>
      <input id={id} className={styles.checkbox} type="checkbox" />
      <label htmlFor={id} className={styles.label}>
        {props.title} <Chevron />
      </label>
      <div className={styles.body}>{props.children}</div>
    </div>
  );
};

export default Expander;
