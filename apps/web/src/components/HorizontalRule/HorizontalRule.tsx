import React, { CSSProperties } from "react";
import styles from "./HorizontalRule.module.css";

interface HorizontalRule {
  style?: CSSProperties;
}

const HorizontalRule: React.FC<HorizontalRule> = (props) => (
  <hr style={props.style} className={styles.root} />
);

export default HorizontalRule;
