import React from "react";
import styles from "./RadioGroup.module.css";

interface RadioButtonProps {
  id?: string;
  value: string;
  checked: boolean;
  onSelect: (value: string) => void;
}
const Radio: React.FC<RadioButtonProps> = (props) => {
  const id = props.id ?? props.value;
  return (
    <div className={styles.radio}>
      <input
        className={styles.input}
        value={props.value}
        checked={props.checked}
        onChange={({ target: { value } }) => props.onSelect(value)}
        id={id}
        type="radio"
      />
      <label htmlFor={id}>{props.children}</label>
    </div>
  );
};

export default Radio;
