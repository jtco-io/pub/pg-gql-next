import React from "react";
import styles from "./RadioGroup.module.css";

const RadioGroup: React.FC<{ label: string }> = (props) => (
  <div className={styles.root}>
    <label>{props.label}</label>
    {props.children}
  </div>
);

export default RadioGroup;
