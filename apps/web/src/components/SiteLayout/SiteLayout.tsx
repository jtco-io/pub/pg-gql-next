import React from "react";
import styles from "./SiteLayout.module.css";
import useViewer from "@hooks/useViewer";
import routeMap from "@routeMap";
import Popover from "@components/PopOver/PopOver";
import Link from "@components/Link";

const SiteLayout: React.FC = ({ children }) => {
  const viewer = useViewer();
  return (
    <div className={styles.container}>
      <nav className={styles.navbar}>
        <Link href="/">Home</Link>
        <div className={styles.rightNavbar}>
          {viewer ? (
            <>
              {viewer.isAdmin && <Link {...routeMap.admin.index}>Admin</Link>}
              <Popover id="menu" text="Menu" className={styles.authPopover}>
                <Link {...routeMap.account.profile}>Account</Link>
                <Link {...routeMap.auth.logout}>Logout</Link>
              </Popover>
            </>
          ) : (
            <>
              <Link {...routeMap.auth.login}>Login</Link>
              <Link {...routeMap.auth.registration}>Register</Link>
            </>
          )}
        </div>
      </nav>

      <main className={styles.main}>{children}</main>

      <footer className={styles.footer}>Footer</footer>
    </div>
  );
};

export default SiteLayout;
