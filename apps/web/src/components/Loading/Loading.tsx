import React from "react";

const Loading: React.FC<{ loading: boolean }> = (props) => {
  if (props.loading) {
    return <div>Loading...</div>;
  }
  return <>{props.children}</>;
};

export default Loading;
