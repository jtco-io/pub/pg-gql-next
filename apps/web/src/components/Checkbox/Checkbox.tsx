import React from "react";
import styles from "./Checkbox.module.css";

interface CheckboxProps {
  id: string;
  checked: boolean;
  onChange?: (value: boolean) => void;
  rootDivProps?: React.DetailedHTMLProps<
    React.HTMLAttributes<HTMLDivElement>,
    HTMLDivElement
  >;
  labelProps?: React.DetailedHTMLProps<
    React.LabelHTMLAttributes<HTMLLabelElement>,
    HTMLLabelElement
  >;
  checkboxProps?: React.DetailedHTMLProps<
    React.InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  >;
}

const Checkbox: React.FC<CheckboxProps> = (props) => (
  <div {...props.rootDivProps}>
    <input
      className={styles.input}
      onChange={() => props.onChange && props.onChange(!props.checked)}
      {...props.checkboxProps}
      id={props.id}
      type="checkbox"
    />
    <label {...props.labelProps} htmlFor={props.id}>
      {props.children}
    </label>
  </div>
);

export default Checkbox;
