import { useEffect } from "react";
import { useRouter } from "next/router";
import { useViewerAccountQuery } from "../../graphql-types";
import routes from "../routeMap";

export function useAdmin() {
  const router = useRouter();
  const { data, loading } = useViewerAccountQuery();
  useEffect(() => {
    if (!loading && !data?.viewer) {
      router.push(routes.landing.href);
    }
  }, [router, loading, data]);
}

export default useAdmin;
