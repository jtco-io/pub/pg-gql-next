import React, { Context, useContext, useState } from "react";
import { Client } from "@utils/apollo";
import { ViewerDocument, ViewerQuery, ViewerQueryVariables } from "@gqlgen";

export type Viewer = ViewerQuery["viewer"];

const ViewerContext = React.createContext<Viewer | undefined>(undefined);

/**
 * Watches for any changes to the viewer query
 * and returns the viewer object from the Apollo cache.
 */
export function useViewerObservable(
  apolloClient: Client
): [Viewer, Context<Viewer>] {
  const [viewer, setViewer] = useState<Viewer>();
  // only run on client
  if (typeof window !== "undefined") {
    apolloClient
      .watchQuery<ViewerQuery, ViewerQueryVariables>({
        query: ViewerDocument,
      })
      .result()
      .then(({ data: { viewer } }) => setViewer(viewer));
  }

  return [viewer, ViewerContext];
}

const useViewer = () => useContext(ViewerContext);

export default useViewer;
