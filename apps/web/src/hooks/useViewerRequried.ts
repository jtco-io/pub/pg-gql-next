import { useRouter } from "next/router";
import { useViewerQuery } from "@gqlgen";
import routeMap from "@routeMap";

/**
 * Handles redirecting to login page if viewer is undefined
 * Redirects back to previous page when user
 */
export function useViewerRequired(adminRequired = false) {
  const router = useRouter();
  const { data, loading } = useViewerQuery();
  const viewer = data?.viewer;
  if (!loading) {
    if (!viewer || (viewer && adminRequired && !viewer.isAdmin)) {
      router.push(
        `${routeMap.auth.login.href}?redirectPath=${router.asPath.replaceAll(
          "%2F",
          "/"
        )}`
      );
    }
  }

  return loading;
}
