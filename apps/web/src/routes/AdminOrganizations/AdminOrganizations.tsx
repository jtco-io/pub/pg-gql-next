import React from "react";
import { useRouter } from "next/router";
import { useAdminOrganizationsQuery } from "@gqlgen";
import Table from "@components/Table";
import routes from "@routeMap";

const AdminOrganizations: React.FC = () => {
  const router = useRouter();
  const { data, loading } = useAdminOrganizationsQuery();
  return (
    <>
      <Table
        columnKeys={["name", "Members Count", "View"]}
        keyColumn={"id"}
        rows={data?.organizations?.nodes.reduce<Record<string, any>[]>(
          (acc, node) => {
            console.log(routes.admin.organization(node.slug));
            acc.push({
              ...node,
              "Members Count": node.organizationMemberships.totalCount,
              View: (
                <button
                  onClick={() =>
                    router.push(routes.admin.organization(node.slug))
                  }
                >
                  View
                </button>
              ),
            });
            return acc;
          },
          []
        )}
      />
      F
    </>
  );
};
export default AdminOrganizations;
