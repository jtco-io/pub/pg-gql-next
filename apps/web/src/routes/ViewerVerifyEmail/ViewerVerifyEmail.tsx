import React, { useEffect } from "react";
import { useRouter } from "next/router";
import { useViewerVerifyEmailMutation } from "@gqlgen";

const ViewerVerifyEmail: React.FC = (props) => {
  const router = useRouter();
  const userEmailId = router.query.id?.toString();
  const token = router.query.token?.toString();
  const [mutate, mutation] = useViewerVerifyEmailMutation({
    errorPolicy: "all",
  });

  useEffect(() => {
    if (userEmailId && token) {
      mutate({ variables: { input: { userEmailId, token } } });
    }
  }, [mutate, userEmailId, token]);

  if (mutation.loading || !userEmailId || !token) {
    return <div>Loading</div>;
  }

  if (mutation.error) {
    return <div>Error!</div>;
  }

  return <span>Email successfully registered</span>;
};

export default ViewerVerifyEmail;
