import React, { useEffect } from "react";
import { useAdminOrganizationLazyQuery } from "../../../graphql-types";
import { Table, TableRow } from "../../components/Table";
import { useRouter } from "next/router";

const AdminOrganization: React.FC = () => {
  const router = useRouter();
  const slug = router.query.slug?.toString();
  const [query, { data, loading }] = useAdminOrganizationLazyQuery();
  useEffect(() => {
    slug && query({ variables: { slug } });
  }, [query, slug]);
  const org = data?.organizationBySlug;
  if (loading || !slug) {
    return <>Loading</>;
  }
  return (
    <>
      <h1>Organization: {org?.name}</h1>
      <Table
        columnKeys={["name"]}
        keyColumn={"id"}
        rows={org?.organizationMemberships.nodes.reduce<
          Record<string, string>[]
        >((acc, node) => {
          acc.push({ id: node.id, name: node.user!.name as string });
          return acc;
        }, [])}
      />
    </>
  );
};
export default AdminOrganization;
