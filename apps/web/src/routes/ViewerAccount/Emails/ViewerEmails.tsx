import React, { useState } from "react";
import {
  useViewerEmailsAddEmailMutation,
  useViewerAccountEmailsQuery,
  ViewerAccountEmailsDocument,
  ViewerAccountEmailsQuery,
} from "@gqlgen";
import Form from "@components/Form";
import Table from "@components/Table";
import Checkbox from "@components/Checkbox";
import TextField from "@components/TextField";

// const ViewerEmailCreate: React.FC = () => {
//   const [isOpen, toggleOpen] = useState(true);
//   const [state, setState] = useState("");
//   const [mutate] = useViewerEmailsAddEmailMutation({
//     errorPolicy: "all",
//     update(cache, { data }) {
//       const email = data?.createUserEmail?.userEmail;
//       if (email) {
//         cache.updateQuery<ViewerAccountEmailsQuery>(
//           { query: ViewerAccountEmailsDocument },
//           (cachedData) => {
//             const viewer = cachedData?.viewer;
//             if (viewer?.userEmails) {
//               viewer?.userEmails.nodes.push(email);
//               return cachedData;
//             }
//             return cachedData;
//           }
//         );
//       }
//     },
//   });
//
//   return (
//     <Form
//       onSubmit={() =>
//         mutate({ variables: { input: { userEmail: { email: state } } } })
//       }
//       actions={
//         <button onClick={() => toggleOpen(!isOpen)}>
//           {isOpen ? "Close" : "Create Email"}
//         </button>
//       }
//     >
//       {isOpen && (
//         <TextField
//           id="email"
//           label="email"
//           type="email"
//           value={state}
//           onChange={setState}
//           autoComplete="email"
//         />
//       )}
//     </Form>
//   );
// };

const ViewerEmails: React.FC = (props) => {
  const { data } = useViewerAccountEmailsQuery();
  const emails = data?.viewer?.userEmails.nodes || [];
  console.log(data);

  return (
    <>
      <h3>Emails</h3>
      {emails && (
        <Table
          keyColumn="id"
          columnKeys={["Address", "Primary", "Verified", "Delete"]}
          rows={emails.map((node) => ({
            id: node.id,
            address: node.email,
            primary: <Checkbox id={node.id} checked={node.isPrimary} />,
            verified: <Checkbox id={node.id} checked={node.isVerified} />,
            delete: (
              <button onClick={() => console.log("CLICK!")}>Delete</button>
            ),
          }))}
        />
      )}
      {/*<ViewerEmailCreate />*/}
    </>
  );
};
export default ViewerEmails;
