import React, { useEffect, useState } from "react";
import {
  useViewerAccountQuery,
  useViewerAccountUpdateViewerMutation,
  ViewerAccountUpdateViewerMutationVariables,
} from "@gqlgen";
import TextField from "@components/TextField";
import Form from "@components/Form";
import { Card, CardActions, CardContent, CardHeader } from "@components/Card";
import Button from "@components/Button";

const Route: React.FC = () => {
  const query = useViewerAccountQuery();
  const viewer = query.data?.viewer;
  const [state, setState] = useState<
    ViewerAccountUpdateViewerMutationVariables["input"]
  >({ id: viewer?.id, patch: {} });

  const [mutate, mutation] = useViewerAccountUpdateViewerMutation({
    variables: { input: state },
    errorPolicy: "all",
  });

  useEffect(() => {
    if (!query.loading && viewer && viewer.id !== state.id) {
      setState({ ...state, id: viewer.id, patch: viewer });
    }
  }, [query.loading, state, viewer]);

  return (
    <Card>
      <CardHeader title={`Edit Profile: ${viewer?.username}`} />
      <CardContent
        style={{
          minWidth: "600px",
        }}
      >
        <Form onSubmit={mutate}>
          <TextField
            id="name"
            label="Name"
            value={state.patch.name || ""}
            onChange={(name) => {
              setState({ ...state, patch: { ...state, name } });
            }}
          />
        </Form>
      </CardContent>
      <CardActions style={{ display: "flex", flexDirection: "row-reverse" }}>
        <Button variant="text" type="submit">
          Save
        </Button>
      </CardActions>
      {/*<ViewerEmails viewer={viewer} />*/}
    </Card>
  );
};

export default Route;
