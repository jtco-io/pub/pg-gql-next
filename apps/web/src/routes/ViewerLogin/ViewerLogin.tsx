import React, { useState } from "react";
import { useRouter } from "next/router";
import {
  LoginInput,
  useLoginMutation,
  ViewerDocument,
  ViewerQuery,
} from "@gqlgen";
import TextField from "@components/TextField";
import Form, { FormError, FormErrors } from "@components/Form";
import Link from "@components/Link";
import getErrors from "./Errors";
import { Card, CardActions, CardContent, CardHeader } from "@components/Card";
import Button from "@components/Button";
import routeMap from "@routeMap";

const ViewerLogin: React.FC = () => {
  const router = useRouter();
  const redirectPath = router.query.redirectPath as string;
  const [state, setState] = useState<LoginInput>({
    username: "",
    password: "",
  });
  const [loginMutation, { error }] = useLoginMutation({
    variables: { input: state },
    errorPolicy: "all",
    async update(cache, { data }) {
      const viewer = data?.login?.user;
      if (viewer) {
        cache.writeQuery<ViewerQuery>({
          query: ViewerDocument,
          data: { __typename: "Query", viewer },
        });
        await router.push(redirectPath || routeMap.account.profile.href);
      }
    },
  });

  return (
    <Card>
      <CardHeader title="Login" />
      <Form onSubmit={loginMutation}>
        <CardContent>
          {error && (
            <FormErrors>
              {getErrors(error.graphQLErrors).map((error) => (
                <FormError key={error}>{error}</FormError>
              ))}
            </FormErrors>
          )}
          <TextField
            id="username"
            label="Username"
            value={state.username}
            onChange={(v) => {
              setState({ ...state, username: v });
            }}
            autoComplete="username"
            required
          />
          <TextField
            id="password"
            label="Password"
            type="password"
            value={state.password}
            onChange={(v) => {
              setState({ ...state, password: v });
            }}
            autoComplete="current-password"
            required
          />
        </CardContent>
        <CardActions
          style={{ display: "flex", justifyContent: "space-between" }}
        >
          <Link {...routeMap.auth.registration}>Create an account</Link>
          <Button variant="text" type="submit">
            Login
          </Button>
        </CardActions>
      </Form>
    </Card>
  );
};

export default ViewerLogin;
