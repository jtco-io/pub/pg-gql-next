import { ApolloError } from "@apollo/client/errors";

enum ErrorMessage {
  CREDS = "CREDS",
}

const errorMessages = {
  [ErrorMessage.CREDS]: "Incorrect username/password",
};

export default function getErrors(graphqlErrors: ApolloError["graphQLErrors"]) {
  const errors = [];

  for (const error of graphqlErrors) {
    switch (error.extensions?.exception.code) {
      case ErrorMessage.CREDS:
        errors.push(errorMessages[ErrorMessage.CREDS]);
        break;
    }
  }
  return errors;
}
