import { ApolloError } from "@apollo/client/errors";

enum ErrorMessage {
  WEAK_PASSWORD = "WEAKP",
  CONFLICT = "NUNIQ",
}

const errorMessages = {
  [ErrorMessage.WEAK_PASSWORD]: "Choose a stronger password",
  [ErrorMessage.CONFLICT]: "This username or email is already taken",
};

export default function getErrors(graphqlErrors: ApolloError["graphQLErrors"]) {
  const errors = [];

  for (const error of graphqlErrors) {
    switch (error.extensions?.exception.code) {
      case ErrorMessage.WEAK_PASSWORD:
        errors.push(errorMessages[ErrorMessage.WEAK_PASSWORD]);
        break;
      case ErrorMessage.CONFLICT:
        errors.push(errorMessages[ErrorMessage.CONFLICT]);
        break;
    }
  }
  return errors;
}
