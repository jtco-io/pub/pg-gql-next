import React, { useState } from "react";
import { useRouter } from "next/router";
import {
  RegisterInput,
  useRegisterMutation,
  ViewerDocument,
  ViewerQuery,
} from "@gqlgen";
import routes from "@routeMap";
import TextField from "@components/TextField";
import Form, { FormError, FormErrors } from "@components/Form";
import getErrors from "./Errors";
import Link from "@components/Link";
import { Card, CardActions, CardContent, CardHeader } from "@components/Card";
import routeMap from "@routeMap";
import Button from "@components/Button";

const ViewerRegistration: React.FC = () => {
  const router = useRouter();
  const [state, setState] = useState<RegisterInput>({
    email: "",
    username: "",
    password: "",
  });
  const [registerMutation, { loading, error }] = useRegisterMutation({
    variables: { input: state },
    errorPolicy: "all",
    async update(cache, { data }) {
      const viewer = data?.register?.user;
      if (viewer) {
        cache.writeQuery<ViewerQuery>({
          query: ViewerDocument,
          data: { __typename: "Query", viewer },
        });
        await router.push(routeMap.account.profile.href);
      }
    },
  });

  return (
    <Card>
      <CardHeader title="Register" />
      <Form onSubmit={registerMutation}>
        <CardContent>
          {error && (
            <FormErrors>
              {getErrors(error.graphQLErrors).map((error) => (
                <FormError key={error}>{error}</FormError>
              ))}
            </FormErrors>
          )}
          <TextField
            id="username"
            label="Username"
            value={state.username}
            onChange={(v) => {
              setState({ ...state, username: v });
            }}
            autoComplete="username"
            required
          />
          <TextField
            id="email"
            label="Email"
            type="email"
            value={state.email}
            onChange={(v) => {
              setState({ ...state, email: v });
            }}
            autoComplete="email"
            required
          />
          <TextField
            id="password"
            label="Password"
            type="password"
            value={state.password}
            onChange={(v) => {
              setState({ ...state, password: v });
            }}
            autoComplete="current-password"
            required
          />
        </CardContent>
        <CardActions
          style={{ display: "flex", justifyContent: "space-between" }}
        >
          <Link {...routes.auth.login}>Already have an account?</Link>

          <Button variant="text" type="submit">
            Register
          </Button>
        </CardActions>
      </Form>
    </Card>
  );
};

export default ViewerRegistration;
