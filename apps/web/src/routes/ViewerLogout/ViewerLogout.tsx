import React, { useEffect } from "react";
import { useRouter } from "next/router";
import { useViewerLogoutMutationMutation } from "@gqlgen";
import useViewer from "@hooks/useViewer";
import { apolloClient } from "@utils/apollo";
import routeMap from "@routeMap";

const ViewerLogout: React.FC = () => {
  const router = useRouter();
  const viewer = useViewer();
  const [logout] = useViewerLogoutMutationMutation({
    async update() {
      await apolloClient.clearStore();
      await router.push(routeMap.auth.login.href);
    },
  });
  useEffect(() => {
    if (viewer) {
      logout();
    } else {
      router.push(routeMap.auth.login.href);
    }
  }, [logout, router, viewer]);

  return <div>Logout</div>;
};

export default ViewerLogout;
