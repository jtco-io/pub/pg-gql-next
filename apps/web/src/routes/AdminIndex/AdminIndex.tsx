import { useRouter } from "next/router";
import React from "react";
import AdminOrganizations from "../AdminOrganizations/AdminOrganizations";

const Route: React.FC = () => {
  return (
    <>
      <h1>Admin Index</h1>
      <AdminOrganizations />
    </>
  );
};
export default Route;
