const dotenv = require("dotenv");

dotenv.config({ path: "../../.env" });

const {
  BACKEND_PORT = "4000",
  GRAPHQL_URL_CLIENT = `http://localhost:${BACKEND_PORT}/graphql`,
  GRAPHQL_URL_SERVER = GRAPHQL_URL_CLIENT,
} = process.env;

module.exports = {
  reactStrictMode: true,
  serverRuntimeConfig: {
    GRAPHQL_URL: GRAPHQL_URL_SERVER,
  },
  publicRuntimeConfig: {
    GRAPHQL_URL: GRAPHQL_URL_CLIENT,
  },
  i18n: {
    // These are all the locales you want to support in
    // your application
    locales: ["en"],
    // This is the default locale you want to be used when visiting
    // a non-locale prefixed path e.g. `/hello`
    defaultLocale: "en",
  },
};
