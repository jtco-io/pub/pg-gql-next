import dotenv from "dotenv";
import { ClientConfig as DatabaseClientConfig } from "pg";
import fs from "fs";

const dotenvPath = "../../.env";
if (fs.existsSync(dotenvPath)) {
  const res = dotenv.config({ path: "../../.env" });
  if (res.error) {
    throw res.error;
  }
}

const {
  DATABASE_HOST = "localhost",
  DATABASE_PORT = "5432",
  DATABASE_NAME,
  DATABASE_OWNER_PASSWORD,
  DATABASE_OWNER_ROLE = `${DATABASE_NAME}_owner`,
} = process.env;

export const databaseDefaults: DatabaseClientConfig = {
  host: DATABASE_HOST,
  port: parseInt(DATABASE_PORT),
  database: DATABASE_NAME,
};

const config = {
  projectName: "pg-gql-next",
  emailLegalText: "",
  fromEmail: "",
  database: {
    ownerConfig: {
      ...databaseDefaults,
      user: DATABASE_OWNER_ROLE,
      password: DATABASE_OWNER_PASSWORD,
    },
  },
};

export function getDatabaseURI({
  user,
  password,
  host,
  port,
  database,
}: DatabaseClientConfig): string {
  return `postgres://${user}:${password}@${host}:${port}/${database}`;
}

export default config;
