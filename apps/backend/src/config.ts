import dotenv from "dotenv";
import { ClientConfig as DatabaseClientConfig } from "pg";
import fs from "fs";

const dotenvPath = "../../.env";
if (fs.existsSync(dotenvPath)) {
  const res = dotenv.config({ path: "../../.env" });
  if (res.error) {
    throw res.error;
  }
}

export enum Environment {
  production = "production",
  development = "development",
  test = "test",
}

interface Config {
  environment: Environment;
  ports: { backend: number; web: number };
  secret?: string;
  database: {
    rootConfig: DatabaseClientConfig;
    ownerConfig: DatabaseClientConfig;
    ownerUri?: string;
    authConfig: DatabaseClientConfig;
    authUri?: string;
    visitorRole: string;
  };
  graphiql: boolean;
}

const {
  NODE_ENV = Environment.development,
  WEB_PORT = "3000",
  BACKEND_PORT = "4000",
  SECRET,
  DATABASE_HOST = "localhost",
  DATABASE_PORT = "5432",
  DATABASE_NAME,
  DATABASE_ROOT_USER,
  DATABASE_ROOT_PASSWORD,
  DATABASE_OWNER_PASSWORD,
  DATABASE_OWNER_URL,
  DATABASE_OWNER_ROLE = `${DATABASE_NAME}_owner`,
  DATABASE_AUTHENTICATOR_ROLE = `${DATABASE_NAME}_authenticator`,
  DATABASE_AUTHENTICATOR_URL,
  DATABASE_VISITOR_ROLE = `${DATABASE_NAME}_visitor`,
  ENABLE_GRAPHIQL = "true",
} = process.env;

export const databaseDefaults: DatabaseClientConfig = {
  host: DATABASE_HOST,
  port: parseInt(DATABASE_PORT),
  database: DATABASE_NAME,
};

const config: Config = {
  environment: NODE_ENV as Environment,
  ports: { web: parseInt(WEB_PORT), backend: parseInt(BACKEND_PORT) },
  secret: SECRET,
  database: {
    rootConfig: {
      ...databaseDefaults,
      user: DATABASE_ROOT_USER,
      password: DATABASE_ROOT_PASSWORD,
      database: "postgres",
    },
    ownerConfig: {
      ...databaseDefaults,
      user: DATABASE_OWNER_ROLE,
      password: DATABASE_OWNER_PASSWORD,
    },
    ownerUri: DATABASE_OWNER_URL,
    authConfig: {
      ...databaseDefaults,
      user: DATABASE_AUTHENTICATOR_ROLE,
    },
    authUri: DATABASE_AUTHENTICATOR_URL,
    visitorRole: DATABASE_VISITOR_ROLE,
  },
  graphiql: ENABLE_GRAPHIQL === "true",
};

export function getDatabaseURI({
  user,
  password,
  host,
  port,
  database,
}: DatabaseClientConfig): string {
  return `postgres://${user}:${password}@${host}:${port}/${database}`;
}

export default config;
