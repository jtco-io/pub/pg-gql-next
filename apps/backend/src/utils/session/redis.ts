/* eslint-disable */
// @ts-nocheck
import ConnectRedis from "connect-redis";
import session from "express-session";
import * as redis from "redis";

const RedisStore = ConnectRedis(session);

export default new RedisStore({
  client: redis.createClient({
    url: process.env.REDIS_URL,
  }),
});
