/* eslint-disable */
// @ts-nocheck
import ConnectPgSimple from "connect-pg-simple";
import session from "express-session";
import { Pool } from "pg";

const PgStore = ConnectPgSimple(session);

export default (rootPgPool: Pool) =>
  new PgStore({
    /*
     * Note even though "connect-pg-simple" lists "pg@7.x" as a dependency,
     * it doesn't `require("pg")` if we pass it a pool. It's usage of the pg
     * API is small; so it's compatible with pg@8.x.
     */
    pool: rootPgPool,
    schemaName: "app_private",
    tableName: "connect_pg_simple_sessions",
  });
