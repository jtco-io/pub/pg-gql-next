#!/usr/bin/env node
/* eslint-disable no-console */
import chalk from "chalk";
import { createServer } from "http";
import { makeApp } from "./app";
import config from "./config";

async function main() {
  // Create our HTTP server
  const httpServer = createServer();

  // Make our application (loading all the middleware, etc)
  const app = await makeApp();

  // Add our application to our HTTP server
  httpServer.addListener("request", app);

  // And finally, we open the listen port
  const PORT = config.ports.backend;
  httpServer.listen(PORT, () => {
    console.log();
    console.log(
      chalk.green(`backend server listening on port ${chalk.bold(PORT)}`)
    );
    console.log();
    console.log(
      `  GraphiQL: ${chalk.bold.underline(`http://localhost:${PORT}/graphiql`)}`
    );
    console.log();
  });
}

main().catch((e) => {
  console.error("Fatal error occurred starting server!");
  console.error(e);
  process.exit(101);
});
