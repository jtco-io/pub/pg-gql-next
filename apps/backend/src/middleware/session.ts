import { Express, RequestHandler } from "express";
import session from "express-session";
import config from "../config";
import { getRootPgPool } from "./database";

const MILLISECOND = 1;
const SECOND = 1000 * MILLISECOND;
const MINUTE = 60 * SECOND;
const HOUR = 60 * MINUTE;
const DAY = 24 * HOUR;

if (!config.secret) {
  throw new Error("Server misconfigured");
}
const MAXIMUM_SESSION_DURATION_IN_MILLISECONDS =
  parseInt(process.env.MAXIMUM_SESSION_DURATION_IN_MILLISECONDS || "", 10) ||
  3 * DAY;

export default (app: Express) => {
  const rootPgPool = getRootPgPool(app);

  const store = process.env.REDIS_URL
    ? /*
       * Using redis for session storage means the session can be shared across
       * multiple Node.js instances (and survives a server restart), see:
       *
       * https://medium.com/mtholla/managing-node-js-express-sessions-with-redis-94cd099d6f2f
       */
      // eslint-disable-next-line @typescript-eslint/no-var-requires
      require("../utils/session/redis").default
    : /*
       * Using PostgreSQL for session storage is easy to set up, but increases
       * the load on your database. We recommend that you graduate to using
       * redis for session storage when you're ready.
       */
      // eslint-disable-next-line @typescript-eslint/no-var-requires
      require("../utils/session/postgres").default(rootPgPool);

  const sessionMiddleware = session({
    rolling: true,
    saveUninitialized: false,
    resave: false,
    cookie: {
      maxAge: MAXIMUM_SESSION_DURATION_IN_MILLISECONDS,
      httpOnly: true, // default
      sameSite: "lax", // Cannot be 'strict' otherwise OAuth won't work.
      secure: "auto", // May need to app.set('trust proxy') for this to work.
    },
    store,
    secret: config.secret as string,
  });

  /**
   * For security reasons we only enable sessions for requests within the our
   * own website; external URLs that need to issue requests to us must use a
   * different authentication method such as bearer tokens.
   */
  const wrappedSessionMiddleware: RequestHandler = (req, res, next) => {
    if (req.isSameOrigin) {
      sessionMiddleware(req, res, next);
    } else {
      next();
    }
  };

  app.use(wrappedSessionMiddleware);
  // getWebsocketMiddlewares(app).push(wrappedSessionMiddleware);
};
