import { Express } from "express";
import passport from "passport";
// import installPassportStrategy from "./passportStrategy";

interface DbSession {
  session_id: string;
}

declare global {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace Express {
    interface User {
      session_id: string;
    }
  }
}

export default async (app: Express) => {
  passport.serializeUser((sessionObject: DbSession, done) => {
    done(null, sessionObject.session_id);
  });

  passport.deserializeUser((session_id: string, done) => {
    done(null, { session_id });
  });

  const passportInitializeMiddleware = passport.initialize();
  app.use(passportInitializeMiddleware);

  const passportSessionMiddleware = passport.session();
  app.use(passportSessionMiddleware);

  // app.get("/logout", (req, res) => {
  //   req.logout();
  //   // res.redirect("/");
  // });
  // if (process.env.GITHUB_KEY) {
  //   await installPassportStrategy(
  //       app,
  //       "github",
  //       GitHubStrategy,
  //       {
  //         clientID: process.env.GITHUB_KEY,
  //         clientSecret: process.env.GITHUB_SECRET,
  //         scope: ["user:email"],
  //       },
  //       {},
  //       async (profile, _accessToken, _refreshToken, _extra, _req) => ({
  //         id: profile.id,
  //         displayName: profile.displayName || "",
  //         username: profile.username,
  //         avatarUrl: get(profile, "photos.0.value"),
  //         email: profile.email || get(profile, "emails.0.value"),
  //       }),
  //       ["token", "tokenSecret"]
  //   );
};
