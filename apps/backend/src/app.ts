import express, { Express } from "express";
import { Server } from "http";
import * as middleware from "./middleware";
import cors from "cors";
import config, { Environment } from "./config";

// Server may not always be supplied, e.g. where mounting on a sub-route
export function getHttpServer(app: Express): Server | void {
  return app.get("httpServer");
}

export async function makeApp(): Promise<Express> {
  /*
   * Our Express server
   */
  const app = express();
  if (
    config.environment === Environment.development ||
    config.environment === Environment.test
  ) {
    app.use(
      cors({
        origin: `http://localhost:${config.ports.web}`,
        credentials: true,
      })
    );
  }

  await middleware.database(app);
  await middleware.sameOrigin(app);
  await middleware.session(app);
  await middleware.passport(app);
  await middleware.postgraphile(app);

  app.get("/healthcheck", (req, res) => {
    res.send("Healthy!");
  });

  /*
   * Error handling middleware
   */
  // await middleware.installErrorHandler(app);

  return app;
}
