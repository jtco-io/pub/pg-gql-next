const dotenv = require("dotenv");

dotenv.config({ path: ".env" });

const {
  DATABASE_HOST = "localhost",
  DATABASE_PORT = 5432,
  DATABASE_NAME,
  DATABASE_ROOT_USER,
  DATABASE_ROOT_PASSWORD,
  DATABASE_OWNER_ROLE = `${DATABASE_NAME}_owner`,
  DATABASE_OWNER_PASSWORD,
  DATABASE_AUTHENTICATOR_ROLE = `${DATABASE_NAME}_authenticator`,
  DATABASE_VISITOR_ROLE = `${DATABASE_NAME}_visitor`,
} = process.env;

function getDatabaseURI({ user, password, host, port, database }) {
  return `postgres://${user}:${password}@${host}:${port}/${database}`;
}

module.exports = {
  rootConnectionString: getDatabaseURI({
    user: DATABASE_ROOT_USER,
    password: DATABASE_ROOT_PASSWORD,
    host: DATABASE_HOST,
    port: DATABASE_PORT,
    database: "postgres",
  }),
  connectionString: getDatabaseURI({
    user: DATABASE_OWNER_ROLE,
    password: DATABASE_OWNER_PASSWORD,
    host: DATABASE_HOST,
    port: DATABASE_PORT,
    database: DATABASE_NAME,
  }),
  shadowConnectionString: getDatabaseURI({
    user: DATABASE_OWNER_ROLE,
    password: DATABASE_OWNER_PASSWORD,
    host: DATABASE_HOST,
    port: DATABASE_PORT,
    database: `${DATABASE_NAME}_shadow`,
  }),
  placeholders: {
    ":DATABASE_AUTHENTICATOR": DATABASE_AUTHENTICATOR_ROLE,
    ":DATABASE_VISITOR": DATABASE_VISITOR_ROLE,
    ":DATABASE_TEST_NAME": `${DATABASE_NAME}_test`,
  },
  afterReset: [
    "!scripts/afterReset.sql",
    {
      _: "command",
      command:
        "DATABASE_URL=${GM_DBURL} yarn workspace worker install-db-schema",
    },
  ],
  afterCurrent: [
    "seeds/000-users.sql",
    "seeds/001-organizations.sql",
    // "scripts/testDatabaseDrop.sql",
  ],
};
