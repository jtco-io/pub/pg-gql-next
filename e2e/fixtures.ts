import { test as base } from "@playwright/test";
import getPoms from "./pageObjects";
import {
  DatabaseClient,
  databasePool,
  databaseClientTestFixture,
} from "./utils/database";
import { Pool } from "pg";
import {
  DatabaseRowUser,
  generateUserFixture,
  GenerateUserInput,
} from "./utils/users";

type TestFixtures = {
  poms: ReturnType<typeof getPoms>;
  createUser: (
    userInput?: Partial<GenerateUserInput>
  ) => Promise<DatabaseRowUser>;
  db: DatabaseClient;
};

type WorkerFixtures = {
  databasePoolClient: Pool;
};

const test = base.extend<TestFixtures, WorkerFixtures>({
  databasePoolClient: [
    async ({}, use) => {
      const databaseClient = await databasePool();
      await use(databaseClient);
    },
    { scope: "worker" },
  ],
  db: async ({ databasePoolClient }, use) => {
    const db = await databaseClientTestFixture(databasePoolClient);
    await use(db);
    await db.release();
  },
  poms: ({ page }, use) => use(getPoms(page)),
  createUser: async ({ db }, use, testInfo) => {
    let user, cleanup!: () => Promise<void>;
    await use(async (userInput) => {
      [user, cleanup] = await generateUserFixture(
        db,
        testInfo.workerIndex,
        userInput
      );
      return user;
    });
    await cleanup();
  },
});

export default test;
