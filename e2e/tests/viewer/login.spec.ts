import test from "../../fixtures";

test("viewer can login and logout", async ({ poms, page, createUser }) => {
  const user = await createUser({});
  await page.goto("/");
  await poms.navbar.loginLink.click();
  await poms.viewerLogin.usernameInput.fill(user.username);
  await poms.viewerLogin.passwordInput.fill(user.password);
  await poms.viewerLogin.submitButton.click();
  await page.waitForURL(/account/);
  await poms.navbar.menuDropDown.click();
  await poms.navbar.logoutLink.click();
  await poms.viewerLogin.header.waitFor();
});

test("viewer login form, non existent user", async ({ poms, page }) => {
  await poms.viewerLogin.goto();
  await poms.viewerLogin.usernameInput.fill("nonexistentUser");
  await poms.viewerLogin.passwordInput.fill("test123");
  await poms.viewerLogin.submitButton.click();
  await page.locator("text=Incorrect username/password");
});
