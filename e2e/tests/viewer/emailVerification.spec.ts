import test from "../../fixtures";
import { expect } from "@playwright/test";

interface UserEmail {
  id: string;
  user_id: string;
  email: string;
  is_verified: boolean;
  is_primary: boolean;
}

test("verify email", async ({ page, createUser, db }) => {
  const user = await createUser();
  const {
    rows: [userEmail],
  } = await db.query<UserEmail>(
    `SELECT *
       FROM app_public.user_emails
       WHERE user_id = $1
         AND is_primary = FALSE
         AND is_verified = FALSE`,
    [user.id]
  );
  const {
    rows: [userEmailSecret],
  } = await db.waitForQuery<{
    user_email_id: string;
    verification_token: string;
  }>(
    `SELECT *
       FROM app_private.user_email_secrets
       WHERE user_email_id = $1`,
    [userEmail.id],
    (res) => res.rowCount === 1
  );
  await page.goto(
    `/verify-email?id=${userEmail.id}&token=${userEmailSecret.verification_token}`
  );
  await page
    .locator("span:has-text('Email successfully registered')")
    .waitFor();
  const {
    rows: [userEmailConfirmation],
  } = await db.waitForQuery(
    `SELECT *
       FROM app_public.user_emails
       WHERE email = $1
         AND is_verified = TRUE;`,
    [userEmail.email],
    ({ rowCount }) => rowCount === 1
  );
  expect(userEmailConfirmation.is_primary).toBeTruthy();
  expect(userEmailConfirmation.is_verified).toBeTruthy();
});
