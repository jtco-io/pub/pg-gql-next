import test from "../../fixtures";
import { expect } from "@playwright/test";

test("viewer registration success", async ({ page, poms, db }) => {
  const time = Date.now();
  const username = `user${time}`;
  const email = `${username}@test.com`;
  await page.goto("/");
  await poms.navbar.registrationLink.click();
  await poms.viewerRegistration.submitForm({
    username,
    email,
    password: "strongpassword",
  });
  await page.waitForURL(/account/);
  const usersQuery = await db.waitForQuery(
    `DELETE
       FROM app_public.users
       WHERE username = $1
       RETURNING *`,
    [username],
    (res) => res.rowCount === 1
  );
  expect(usersQuery.rowCount).toBe(1);
});

test("registration error: weak password", async ({ page, poms }) => {
  const time = Date.now();
  const username = `user${time}`;
  const email = `${username}@test.com`;
  await poms.viewerRegistration.goto();
  await poms.viewerRegistration.submitForm({
    username,
    email,
    password: "weak",
  });
  await page.waitForSelector("'Choose a stronger password'");
});

test("registration error: user exists", async ({ page, poms, createUser }) => {
  const user = await createUser({});
  await poms.viewerRegistration.goto();
  await poms.viewerRegistration.submitForm({
    username: user.username,
    email: "test@test.com",
    password: "strongpassword",
  });
  await page.waitForSelector("'This username or email is already taken'");
});
