import { expect, Locator, Page } from "@playwright/test";

export default class NavbarObject {
  readonly page: Page;
  readonly loginLink: Locator;
  readonly registrationLink: Locator;
  readonly menuDropDown: Locator;
  readonly logoutLink: Locator;

  constructor(page: Page) {
    this.page = page;
    this.loginLink = page.locator("text=Login");
    this.registrationLink = page.locator("text=Register");
    this.menuDropDown = page.locator("label:text('Menu')");
    this.logoutLink = page.locator("a:text('Logout')");
  }
}
