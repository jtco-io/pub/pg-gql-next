import { Page } from "@playwright/test";
import NavbarObject from "./navbar";
import ViewerLogin from "./viewerLogin";
import ViewerRegistration from "./viewerRegistration";

export default function getPoms(page: Page) {
  return {
    navbar: new NavbarObject(page),
    viewerLogin: new ViewerLogin(page),
    viewerRegistration: new ViewerRegistration(page),
  };
}
