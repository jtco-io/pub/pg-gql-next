import { Locator, Page } from "@playwright/test";
import { DatabaseRowUser } from "../utils/users";

export default class ViewerLogin {
  readonly page: Page;
  readonly header: Locator;
  readonly usernameInput: Locator;
  readonly passwordInput: Locator;
  readonly submitButton: Locator;
  readonly postLoginLocator: Locator;

  constructor(page: Page) {
    this.page = page;
    this.header = page.locator("span:text('Login')");
    this.usernameInput = page.locator("#username");
    this.passwordInput = page.locator("#password");
    this.submitButton = page.locator("button:text('Login')");
    this.postLoginLocator = page.locator(`title:has-text("Edit Profile")`);
  }

  async goto() {
    await this.page.goto("/login");
  }

  /**
   * Shortcut for tests that require login
   * Should not be an example for other POMs
   * @param user
   */
  async login(user: Pick<DatabaseRowUser, "username" | "password">) {
    await this.goto();
    await this.usernameInput.fill(user.username);
    await this.passwordInput.fill(user.password);
    await this.submitButton.click();
    await this.page.waitForResponse("**/graphql");
    await this.postLoginLocator.waitFor();
  }
}
