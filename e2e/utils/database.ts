import config from "../playwright.config";
import { test } from "@playwright/test";
import { Pool, PoolClient, QueryResult, QueryResultRow } from "pg";
export { PoolClient } from "pg";
const {
  DATABASE_HOST = "localhost",
  DATABASE_PORT = "5432",
  DATABASE_NAME,
  DATABASE_OWNER_ROLE = `${DATABASE_NAME}_owner`,
  DATABASE_OWNER_PASSWORD,
} = process.env;

export const databasePool = () =>
  new Pool({
    user: DATABASE_OWNER_ROLE,
    password: DATABASE_OWNER_PASSWORD,
    database: DATABASE_NAME,
    host: DATABASE_HOST,
    port: Number(DATABASE_PORT),
  });

export interface DatabaseClient extends PoolClient {
  /**
   * This functions is used to repeat a query until conditionToBeTrue returns true
   * By default after ten trys (over one second) we will start logging the attempts
   * @param query
   * @param args
   * @param conditionToBeTrue
   * @param timeout
   */
  waitForQuery<R extends QueryResultRow = any>(
    query: string,
    args: Array<string | number>,
    conditionToBeTrue: (res: QueryResult<R>) => boolean,
    timeout?: number
  ): Promise<QueryResult<R>>;
}

export async function databaseClientTestFixture(pool: Pool) {
  const db = (await pool.connect()) as DatabaseClient;
  db.waitForQuery = async (query, args, conditionToBeTrue, timeout = 100) => {
    let attempt = 0;
    let res = await db.query(query, args);
    const maxAttempt = config.timeout! / timeout;
    while (!conditionToBeTrue(res)) {
      attempt++;
      if (attempt >= maxAttempt) {
        test.expect(conditionToBeTrue(res)).toBeTruthy();
      }
      await new Promise((resolve) => {
        setTimeout(resolve, timeout);
      });
      res = await db.query(query, args);
    }
    return res;
  };
  return db;
}
