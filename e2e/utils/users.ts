import { DatabaseClient } from "./database";

export interface DatabaseRowUser {
  id: string;
  username: string;
  password: string;
  name: string;
  is_admin: boolean;
  is_verified: boolean;
}

export interface GenerateUserInput {
  username: string;
  email: string;
  emailIsVerified: boolean;
  password: string;
  isVerified: boolean;
  isAdmin: boolean;
  syncStripeCustomer: boolean;
}

export async function cleanUpUser(db: DatabaseClient, user: DatabaseRowUser) {
  // Cleanup and rows that must be deleted before user is removed.
  await db.query(
    ` DELETE
        FROM app_public.users
        WHERE id = $1`,
    [user.id]
  );
}

export async function generateUserFixture(
  db: DatabaseClient,
  workerIndex: number,
  userInput?: Partial<GenerateUserInput>
) {
  // Convert utc milliseconds to seconds to fit under 24 character username limit
  const uniqueTimestamp = `${Date.now()}`.slice(-6);
  const username = `e2e_${workerIndex}_${uniqueTimestamp}`;
  const fullUserInput: Partial<GenerateUserInput> = {
    username,
    email: `${username}@test.com`,
    emailIsVerified: false,
    password: "longPassword",
    isVerified: false,
    isAdmin: false,
    ...userInput,
  };

  if (fullUserInput.username!.length > 24) {
    throw "Usernames must be shorter than 24 characters";
  }

  const {
    rows: [user],
  } = await db.query(
    `
        SELECT *
        FROM app_private.really_create_user(username => $1,
                                            email => $2,
                                            email_is_verified => $3,
                                            name => NULL,
                                            avatar_url => NULL,
                                            password => $4)
    `,
    [
      fullUserInput.username,
      fullUserInput.email,
      fullUserInput.emailIsVerified,
      fullUserInput.password,
    ]
  );

  if (fullUserInput.isVerified) {
    await db.query(
      `UPDATE app_public.users
         SET is_verified= TRUE
         WHERE id = $1`,
      [user.id]
    );
  }

  if (fullUserInput.isAdmin) {
    await db.query(
      `UPDATE app_public.users
                    SET is_admin= TRUE
                    WHERE id = $1`,
      [user.id]
    );
  }

  user.password = fullUserInput.password;

  return [user, () => cleanUpUser(db, user)];
}
