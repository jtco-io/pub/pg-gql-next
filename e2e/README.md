## Instillation

In addition to running yarn from the root
we also need to install the playwright browsers.

```bash
npx playwright install
```
