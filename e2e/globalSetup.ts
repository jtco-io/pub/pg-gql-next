import { chromium, FullConfig } from "@playwright/test";
import { ChildProcess, fork, spawn } from "child_process";

async function authenticatedViewer(config: FullConfig) {
  const { baseURL, storageState } = config.projects[0].use;
  const browser = await chromium.launch();
  const page = await browser.newPage();
  await page.goto(baseURL!);
  await page.fill('input[name="user"]', "user");
  await page.fill('input[name="password"]', "password");
  await page.click("text=Sign in");
  await page.context().storageState({ path: storageState as string });
  await browser.close();
}

export default async function globalSetup(config: FullConfig) {
  let backend: ChildProcess, web: ChildProcess, worker: ChildProcess;
  if (process.env.CI) {
    backend = fork("dist/index.js", {
      cwd: "../apps/backend",
    });
    web = fork("node_modules/.bin/next", ["start"], {
      cwd: "../apps/web",
    });
    worker = fork("dist/index.js", {
      cwd: "../apps/worker",
      stdio: "ignore", // TOOD if trying to debug CI worker failures comment this out
    });
  }

  return () => {
    if (process.env.CI) {
      backend.kill();
      web.kill();
      worker.kill();
    }
  };
}
