## Background

Heavily Influenced by [Graphile/Starter](https://github.com/graphile/starter)

This project is simple boilerplate example combining

Backend/GraphQL server

- Database - Postgres
- GraphQL - Postgraphile
- HTTP Server - Express
- Auth Passport

[Database](apps/database/README.md)

- Uses [Graphile Migrate](https://github.com/graphile/migrate)
- Manages the database schema
- Loads and saves data during development

Frontend

- Library -React
- Framework - NextjS
- GraphQL Client - Apollo

Testing/Quality Assurance

- Prettier
- eslint
- Unit/Integration Testing - Jest
- E2E Testing - Playwright

## Getting started

You will need to have

1. Node installed using NVM
1. Postgres installed using `brew`
1. Yarn installed globally

From the root run

1. `yarn` to install dependencies
1. In a separate tab `yarn startpg`
1. `sh ./init` to create database roles.
1. `yarn dev` to start the services

## GraphQL

Typescript Types and GraphQL function calls are automatically generated using [GraphQL Code Generator](https://www.graphql-code-generator.com/)

## E2E

To run a specific test in the E2E suite, pass the test name to grep.

```bash
yarn test --grep "registration success"
```

## Deployment

These [Helm charts](https://github.com/jtco-io/charts/tree/main/charts) are made available for deploying to kubernetes.

### Migrations

Migrations are deployed as a Kubernetes job.

- a preInit container first runs `apps/database/migrations/afterReset.sql` as root and handles
  - initial permissions
  - creates any extensions.
- The main container runs
  - migrations
  - the command can be overridden to also run any data loading you add into the container (think products commited as CSV).
- Dataloading can be also ran here from CSV for examples, drop my a message or issue and I can add to repo

### Backend

- ROOT_URL - https://fullqualifieddomain.com
- DATABASE_OWNER_URL - postgres://graphile_owner:password@postgres-postgresql/graphile
- DATABASE_AUTHENTICATOR_URL - postgres://graphile_authenticator:password@postgres-postgresql/graphile
- DATABASE_VISITOR_ROLE - graphile_visitor
- SECRET - a really long stream of characters

### Web

### Worker

- DATABASE_URL Owner URL - - postgres://graphile_owner:password@postgres-postgresql/graphile

## Package Ideas

- Testing
- Server Auth
-
