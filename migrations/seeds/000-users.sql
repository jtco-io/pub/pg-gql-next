INSERT INTO app_public.users (id, username, name, is_verified, is_admin)
VALUES ('da221b05-a99c-485d-a278-015e7baaeda2', 'jdoe', 'Jane Doe',
        TRUE, FALSE),
       ('23e675b1-d581-4d84-96a9-8f73dbee72ad', 'ncrmro', 'Nicholas Romero', TRUE, TRUE)
ON CONFLICT DO NOTHING;

UPDATE app_private.user_secrets us
SET password_hash = crypt('test123', gen_salt('bf'))
WHERE user_id = (SELECT id FROM app_public.users WHERE username = 'jdoe');

UPDATE app_private.user_secrets us
SET password_hash = crypt('test123', gen_salt('bf'))
WHERE user_id = (SELECT id FROM app_public.users WHERE username = 'ncrmro');


ALTER TABLE app_public.user_emails
    DISABLE TRIGGER ALL;

INSERT INTO app_public.user_emails (id, user_id, email, is_primary, is_verified)
VALUES ('6a3207b2-80fb-4a49-a0a9-b8e74f23e9f4',
        (SELECT id FROM app_public.users WHERE username = 'jdoe'),
        'jdoe@test.com',
        TRUE,
        TRUE),
       ('893b38c2-940d-4f7d-a8ad-395152fbb4ca',
        (SELECT id FROM app_public.users WHERE username = 'ncrmro'),
        'ncrmro@test.com',
        TRUE,
        TRUE),
       ('9c70ed41-2be0-4f0e-8f48-4cdc33c778f5',
        (SELECT id FROM app_public.users WHERE username = 'ncrmro'),
        'ncrmro@coolhost.com',
        FALSE,
        FALSE)
ON CONFLICT DO NOTHING;

ALTER TABLE app_public.user_emails
    ENABLE TRIGGER ALL;
