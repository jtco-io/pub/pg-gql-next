INSERT INTO app_public.organizations (id, name, slug)
VALUES ('cede1279-d3bc-4cfa-ba07-8a4f48cb7415', 'Test Org', 'test-org'),
       ('7b9d97cd-91ba-49ac-8f68-9fa894e01297', 'Big Houston', 'htx-league')
ON CONFLICT DO NOTHING;


INSERT INTO app_public.organization_memberships (organization_id, user_id)
VALUES ((SELECT id FROM app_public.organizations WHERE name = 'Test Org'),
        (SELECT id FROM app_public.users WHERE username = 'jdoe')),
       ((SELECT id FROM app_public.organizations WHERE name = 'Big Houston'),
        (SELECT id FROM app_public.users WHERE username = 'jdoe')),
       ((SELECT id FROM app_public.organizations WHERE name = 'Big Houston'),
        (SELECT id FROM app_public.users WHERE username = 'ncrmro'))
ON CONFLICT DO NOTHING;
