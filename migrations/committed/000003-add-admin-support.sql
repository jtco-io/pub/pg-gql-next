--! Previous: sha1:6b0ba493af18de6d84ee16ba6bc90e8e93bc524c
--! Hash: sha1:fcfb7d5c8aba690e0a85b094097b5599a8d566fc
--! Message: Add admin support

CREATE OR REPLACE FUNCTION app_public.viewer_is_admin() RETURNS app_public.users AS
$$
SELECT *
FROM app_public.users
WHERE id = app_public.current_user_id()
  AND is_admin = TRUE;
$$ LANGUAGE sql STABLE;

DROP POLICY IF EXISTS admin_select_organizations ON app_public.organizations;
CREATE POLICY admin_select_organizations ON app_public.organizations
    FOR SELECT USING (EXISTS(SELECT app_public.viewer_is_admin()));
