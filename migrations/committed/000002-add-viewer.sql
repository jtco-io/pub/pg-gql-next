--! Previous: sha1:fbd6ffb5be53e7e0bfc63929a52cfc13e698c5f0
--! Hash: sha1:6b0ba493af18de6d84ee16ba6bc90e8e93bc524c
--! Message: add viewer

-- Enter migration here

CREATE OR REPLACE  FUNCTION app_public.viewer() RETURNS app_public.users AS
$$
select *
from app_public.users
where id = app_public.current_user_id();
$$ LANGUAGE sql STABLE;

comment on function app_public.viewer() is
  E'The currently logged in user (or null if not logged in).';
