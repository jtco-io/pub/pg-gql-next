ARG BASE_IMAGE=node:18-alpine

FROM $BASE_IMAGE as base
WORKDIR /source

COPY package.json yarn.lock /source/

COPY apps/backend/package.json /source/apps/backend/
COPY apps/web/package.json /source/apps/web/
COPY apps/worker/package.json /source/apps/worker/

RUN yarn install --frozen-lockfile --production=true

FROM $BASE_IMAGE  as built

COPY --from=base /source /source

COPY ./apps /source/apps

