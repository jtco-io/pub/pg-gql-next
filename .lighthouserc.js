const dotenv = require("dotenv");

dotenv.config();

module.exports = {
  ci: {
    collect: {
      startServerCommand: "yarn workspace web start",
      startServerReadyPattern: "started server on",
      url: [
        "http://localhost:3000/",
        "http://localhost:3000/login",
        "http://localhost:3000/register",
      ],
      settings: {
        chromeFlags: "--disable-gpu --no-sandbox --disable-dev-shm-usage",
      },
    },
    assert: {
      preset: "lighthouse:no-pwa",
      assertions: {
        "csp-xss": ["warn", {}],
        "errors-in-console": ["warn", {}],
        "meta-description": ["warn", {}],
        "unused-javascript": ["warn", { maxLength: 0 }],
      },
    },
    upload: {
      // token and server url are set from environment variables and .env file
      target: "lhci",
    },
  },
};
